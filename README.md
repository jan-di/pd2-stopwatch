# Stopwatch

A Payday 2 BLT-Mod which shows your current and best time for every completed objective.

![](docs/example1.png)

The records are tracked separately for the combination of the following things:
- Gamemode
- Difficulty
- One Down enabled
- AI enabled
- Peer Count
- Mod Info (specific, gameplay changing mods. Currently detected: Silent Assassin)

This way, only attempts with the same conditions are compared. Everytime when you complete an objective or the level the chat shows you:
- The time difference to you previous best time under the same conditions
- The previous record under the same conditions `R`
- The all time record under any conditions `AT`

The tracking of new attempts/records is always done at host side. This means, that tracking won't work, if you are playing as client and the host does not have the mod. When you join a lobby, the game will tell you if tracking is enabled or not.

- [Changelog](CHANGELOG.md)
- [Sourcecode](https://gitlab.com/jan-di/pd2-stopwatch)
- [Report an Issue](https://gitlab.com/jan-di/pd2-stopwatch/-/issues)
- [ModWorkshop](https://modworkshop.net/mod/30227)

## Installation

1. Install [SuperBLT Loader](https://superblt.znix.xyz/#installation)
2. Download the [Mod Archive](https://jan-di.gitlab.io/pd2-stopwatch/mod.zip)
3. Extract the files into your mods folder.

## Contributing

See [Contributing Instructions](CONTRIBUTING.md)
