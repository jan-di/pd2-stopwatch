# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2020-12-12
### Added
- When joining a lobby, the mod shows if the host is tracking attempts
### Fixed
- Some cases where the game could crash when sending local chat messages

## [1.1.0] - 2019-10-03
### Changes
- Records are compacted before written on disk
- Much internal code cleanup
### Fixed
- Crash related to big network messages

## [1.0.0] - 2018-08-28
### Added
- Initial Release