std = "luajit"

new_globals = {
    "MenuCallbackHandler"
}

new_read_globals = {
    "Global",
    "ModPath",
    "SavePath",
    "managers",
    "ChatManager",
    "ObjectivesManager",
    "GameStateMachine",
    "DelayedCalls",
    "Network",
    "SystemInfo",
    "Hooks",
    "MenuHelper",
    "QuickMenu",
    "Color",
    "json",
    "file",
    "Idstring",
    "log"
}

ignore = {
    "212/_.*" -- ignore arguments starting with an underscore. needed, to match the api functions
}