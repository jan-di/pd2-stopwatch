#/bin/sh
set -e

export MOD_IDENTIFIER="jan-di.stopwatch"
export MOD_NAME="Stopwatch"
export MOD_DOWNLOAD="https://$CI_PROJECT_NAMESPACE.gitlab.io/$CI_PROJECT_NAME/mod.zip"
export MOD_PATCHNOTES="$CI_PROJECT_URL/-/blob/master/CHANGELOG.md"

apk --no-cache add \
    gettext \
    git \
    rsync \
    zip

mkdir -p .build/$MOD_NAME
rsync -ar --stats --exclude-from '.gitignore' --exclude-from '.gitlab-ci/build_exclude.txt' --prune-empty-dirs . .build/$MOD_NAME
cd .build
zip -r mod.zip $MOD_NAME
cd ..
export MOD_HASH=$(python -c "import hash; print(hash.dir('.build/$MOD_NAME'))")
rm -r .build/$MOD_NAME
envsubst < .gitlab-ci/meta_template.json > .build/meta.json
