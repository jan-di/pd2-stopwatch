local Class = _G.StopwatchMod.Status
local Util = _G.StopwatchMod.Util
local Mod = _G.StopwatchMod.Mod
local Settings = _G.StopwatchMod.Settings
local LuaNetwork = _G.LuaNetworking

Class.RemoteStatus = nil

function Class:onIngameWaitingForPlayers(is_host)
    if is_host then
        local status = Settings:get("is_active")
        DelayedCalls:Add("SW_DisplayHostStatus", 3, function()
            self.writeStatus(status)
        end)
    else
        self.sendStatusQuery()
        DelayedCalls:Add("SW_DisplayClientStatus", 3, function()
            if self.RemoteStatus == nil then
                self.RemoteStatus = false
            end
            self.writeStatus(self.RemoteStatus)
        end)
    end
end

function Class:onReceiveStatusBroadcast(sender, status_json)
    if Network:is_server() or sender ~= 1 then
        Util.log("Received invalid STATUS_BROADCAST message!")
        return
    end

    local status = json.decode(status_json)
    self.RemoteStatus = status
    self.writeStatus(self.RemoteStatus)
end

function Class.onReceiveStatusQuery(sender)
    if not Network:is_server() or sender == 1 then
        Util.log("Received invalid STATUS_QUERY message!")
        return
    end

    local status = Settings:get("is_active")
    Class.sendStatusQueryResponse(sender, status)
end

function Class:onReceiveStatusQueryResponse(sender, status_json)
    if Network:is_server() or sender ~= 1 then
        Util.log("Received invalid STATUS_QUERY_RESPONSE message!")
        return
    end

    local status = json.decode(status_json)
    self.RemoteStatus = status
end

function Class.sendStatusBroadcast(status)
    local status_json = json.encode(status)
    LuaNetwork:SendToPeers(Mod.MESSAGE.STATUS_BROADCAST, status_json)
end

function Class.sendStatusQuery()
    LuaNetwork:SendToPeer(1, Mod.MESSAGE.STATUS_QUERY, "_")
end

function Class.sendStatusQueryResponse(peer, status)
    local status_json = json.encode(status)
    LuaNetwork:SendToPeer(peer, Mod.MESSAGE.STATUS_QUERY_RESPONSE, status_json)
end

function Class.writeStatus(status)
    local color, message

    if status then
        color = Mod.COLOR_GREEN
        message = Util.localize("stopwatch_status_enabled")
    else
        color = Mod.COLOR_RED
        message = Util.localize("stopwatch_status_disabled")
    end
    if not Settings:get("disable_chat") then
        Util.localChatMessage("[Stopwatch]", message, Mod.COLORS[color])
    end
end