local Records = _G.StopwatchMod.Records
local Status = _G.StopwatchMod.Status

Hooks:PostHook(GameStateMachine, "change_state", "Stopwatch_GameStateMachine_Post_change_state", function(_, state)
    if state._name == "victoryscreen" and Network:is_server() then
        Records:completeLevel()
	elseif state._name == "gameoverscreen" or state._name == "menu_main" or state._name == "ingame_lobby_menu" then
        Records:clearPending()
    elseif state._name == "ingame_waiting_for_players" then
        Status:onIngameWaitingForPlayers(Network:is_server())
    end
end)